
let ElementData =
{
	/**
	 * Get data attribute from an object as a valid object
	 * @param  {String} name		The name of the data attribute to pull the data from
	 * @param  {HTMLElement} el		The elemnt to pull the data from
	 * @param  {Object} defaults	Any defaults that should be returned
	 * @return {Object}		  	The returned and formatted object
	 */
	get : function(name, el, defaults = {})
	{
		// get the data attribute from the html element
		let attr				= el.getAttribute('data-' + name),
			options				= {};

		// if the attribute exists
		if(attr)
		{
			try
			{
				// try and convert the data attribute to an object
				options			= JSON.parse(attr);
			}
			// if anything fails, log it
			catch(e)
			{
				console.error(`invalid config data *data-${name}*`, el);
			}
		}

		// return an object complete with any defaults that have been passed in
		return Object.assign(defaults, options);
	},

	/**
	 * Save an object within a data attribute on a specific element
	 * @param  {String} name		The name of the data attribute to save the data from
	 * @param  {HTMLElement} el		The elemnt to save the data to
	 * @param {Object} data			Data object to stors in the data attribute
	 */
	set : function(name, el, data = {})
	{
		// get the congig from the element
		let config				= this.get(name, el);

		// merge the element config with the passed in data
		config					= Object.assign(config, data);

		// update the data attribute with a stringified version of the config
		el.setAttribute("data-" + name, JSON.stringify(config));
	}
};

export {ElementData};
