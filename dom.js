
var closest = (function()
{
	var el		= HTMLElement.prototype;
	var matches	= el.matches || el.webkitMatchesSelector || el.mozMatchesSelector || el.msMatchesSelector;

	return function closest(el, selector)
	{
		try
		{
			return matches.call(el, selector) ? el : closest(el.parentElement, selector);
		}
		catch(e)
		{
			return false;
		}
	};
})();

var dom =
{
	getPageScroll : function()
	{
		if (window.pageYOffset)
		{
			return window.pageYOffset;
		}

		if (document.documentElement && document.documentElement.scrollTop)
		{
			return document.documentElement.scrollTop;
		}

		if (document.body)
		{
			return document.body.scrollTop;
		}

		return false;
	},

	position : function(elm)
	{
		var box					= elm.getBoundingClientRect();

		var body				= document.body;
		var docEl				= document.documentElement;

		var scrollTop			= window.pageYOffset || docEl.scrollTop || body.scrollTop;
		var scrollLeft			= window.pageXOffset || docEl.scrollLeft || body.scrollLeft;

		var clientTop			= docEl.clientTop || body.clientTop || 0;
		var clientLeft			= docEl.clientLeft || body.clientLeft || 0;

		var top					= box.top +  scrollTop - clientTop;
		var left				= box.left + scrollLeft - clientLeft;

		return {
			top		: Math.round(top),
			left	: Math.round(left)
		};
	},

	height : function(elm)
	{
		return elm.clientHeight;
	},

	computedStyle : function(el, selector = null, prop = 'content')
	{
		try
		{
			// get the screen size as defined in the css
			let value			= window.getComputedStyle(el, selector).getPropertyValue(prop);

			if(prop === 'content')
			{
				value			= value.replace(/\"||\'/g, '');
			}

			return value;
		}
		catch(e)
		{
			return '0000';
		}
	},

	/**
	 * Remove an element from the DOM
	 * @param  {Element} elm [description]
	 */
	remove : function(elm)
	{
		if(elm)
		{
			elm.parentNode.removeChild(elm);
		}
	},

	/**
	 * [after description]
	 * @param  {Element} elm		[description]
	 * @param  {[type]} htmlString [description]
	 */
	after : function(elm, htmlString)
	{
		elm.insertAdjacentHTML('afterend', htmlString);
	},

	/**
	 * [insertAfter description]
	 * @param  {Element} elm	 [description]
	 * @param  {[type]} newNode [description]
	 */
	insertAfter : function(elm, newNode)
	{
		elm.parentNode.insertBefore(newNode, elm.nextSibling);
	},

	/**
	 * [append description]
	 * @param  {Element} elm	 [description]
	 * @param  {[type]} newNode [description]
	 */
	append : function(elm, newNode)
	{
		elm.appendChild(newNode);
	},

	/**
	 * [prepend description]
	 * @param  {Element} elm	 [description]
	 * @param  {[type]} newNode [description]
	 */
	prepend : function(elm, newNode)
	{
		elm.insertBefore(newNode, elm.firstChild);
	},

	/**
	 * [before description]
	 * @param  {Element} elm		[description]
	 * @param  {[type]} htmlString [description]
	 */
	before : function(elm, htmlString)
	{
		elm.insertAdjacentHTML('beforebegin', htmlString);
	},

	/**
	 * [clone description]
	 * @param  {Element} elm [description]
	 * @return {[type]}	 [description]
	 */
	clone : function(elm)
	{
		return elm.cloneNode(true);
	},

	/**
	 * [next description]
	 * @param  {[type]}   elm [description]
	 * @return {Function}	 [description]
	 */
	next : function(elm)
	{
		return elm.nextElementSibling;
	},

	/**
	 * [find description]
	 * @param  {Element} elm	  [description]
	 * @param  {[type]} selector [description]
	 * @return {[type]}		  [description]
	 */
	find : function(elm, selector)
	{
		return elm.querySelectorAll(selector);
	},

	/**
	 * [prev description]
	 * @param  {Element} elm [description]
	 * @return {[type]}	 [description]
	 */
	prev : function(elm)
	{
		return elm.previousElementSibling;
	},

	/**
	 * [selectAll description]
	 * @param  {[type]} query [description]
	 * @return {[type]}	   [description]
	 */
	selectAll : function(query, base = document)
	{
		return base.querySelectorAll(query);
	},

	/**
	 * [scrollPosition description]
	 * @return {[type]} [description]
	 */
	scrollPosition : function()
	{
		var doc		= document.documentElement;

		return {
			top		: (window.pageYOffset || doc.scrollTop ) - (doc.clientTop  || 0),
			left	: (window.pageXOffset || doc.scrollLeft) - (doc.clientLeft || 0)
		};
	},

	/**
	 * [create description]
	 * @param  {[type]} str	  [description]
	 * @param  {[type]} location [description]
	 * @return {[type]}		  [description]
	 */
	create : function(str, location)
	{
		var temp			= document.createElement('div');
		temp.innerHTML		= str;

		var children;

		if (!location)
		{
			children		= temp;
		}
		else
		{
			children		= temp.children;

			arr.each(location, function(str, i, name)
			{
				children	= (i == location.length - 1) ? children[str] : children[str].children;
			});
		}

		return children;
	},

	containsClass : (function()
	{
		if(!! document.documentElement.classList)
		{
			/**
			 * [description]
			 * @param  {Element} elm	   [description]
			 * @param  {[type]} className [description]
			 */
			return function(elm, className)
			{
				if(!elm)
				{
					return false;
				}

				return elm.classList.contains(className);
			}
		}

		/**
		 * [description]
		 * @param  {Element} elm	   [description]
		 * @param  {[type]} className [description]
		 */
		return function(elm, className)
		{
			if (!elm || !elm.className)
			{
				return false;
			}

			var re		= new RegExp('(^|\\s)' + className + '(\\s|$)');

			return elm.className.match(re);
		}
	})(),

	addClass : (function()
	{
		if(!! document.documentElement.classList)
		{
			/**
			 * [description]
			 * @param  {Element} elm	   [description]
			 * @param  {[type]} className [description]
			 */
			return function(elm, className)
			{
				if(!elm)
				{
					return false;
				}

				if (className.indexOf(' ') > 0)
				{
					var names	= className.split(' ');

					for (var i = 0, il = names.length; i < il; i++)
					{
						elm.classList.add(names[i]);
					}
				}
				else
				{
					elm.classList.add(className);
				}
			}
		}

		/**
		 * [description]
		 * @param  {Element} elm	   [description]
		 * @param  {[type]} className [description]
		 */
		return function(elm, className)
		{
			if (!elm)
			{
				return false;
			}

			if (! this.containsClass(elm, className))
			{
				elm.className	+= (elm.className ? ' ' : '') + className;
			}
		}
	})(),

	removeClass : (function()
	{
		if(!! document.documentElement.classList)
		{
			/**
			 * [description]
			 * @param  {Element} elm	   [description]
			 * @param  {[type]} className [description]
			 */
			return function(elm, className)
			{
				if(!elm)
				{
					return false;
				}

				elm.classList.remove(className);
			}
		}

		/**
		 * [description]
		 * @param  {Element} elm	   [description]
		 * @param  {[type]} className [description]
		 */
		return function(elm, className)
		{
			if (!elm || !elm.className)
			{
				return false;
			}

			var regexp		= new RegExp('(^|\\s)' + className + '(\\s|$)', 'g');

			elm.className	= elm.className.replace(regexp, '$2');
		}
	})(),

	toggleClass : (function()
	{
		if(!! document.documentElement.classList)
		{
			/**
			 * [description]
			 * @param  {Element} elm	   [description]
			 * @param  {[type]} className [description]
			 */
			return function(elm, className)
			{
				if(!elm)
				{
					return false;
				}

				return elm.classList.toggle(className);
			}
		}

		/**
		 * [description]
		 * @param  {Element} elm	   [description]
		 * @param  {[type]} className [description]
		 */
		return function(elm, className)
		{
			if(!elm)
			{
				return false;
			}

			if (this.containsClass(elm, className))
			{
				this.removeClass(elm, className);
				return false;
			}
			else
			{
				this.addClass(elm, className);
				return true;
			}
		}
	})(),

	addRemove : function(elm, add, remove)
	{
		if(add !== remove)
		{
			this.addClass(elm, add);
			this.removeClass(elm, remove);
		}
	},

	/**
	 * Get the closest matching element up the DOM tree.
	 * @private
	 * @param  {Element} elem	 Starting element
	 * @param  {String}  selector Selector to match against (class, ID, data attribute, or tag)
	 * @return {Boolean|Element}  Returns null if not match found
	 */
	getClosest : function(elm, selector)
	{
		try
		{
			// Variables
			var firstChar			= selector.charAt(0);
			var supports			= 'classList' in document.documentElement;

			var attribute, value;

			// If selector is a data attribute, split attribute from value
			if (firstChar === '[')
			{
				selector			= selector.substr(1, selector.length - 2);
				attribute			= selector.split('=');

				if (attribute.length > 1)
				{
					value			= true;
					attribute[1]	= attribute[1].replace(/"/g, '').replace(/'/g, '');
				}
			}

			// Get closest match
			for (; elm && elm !== document; elm = elm.parentNode) {

				// If selector is a class
				if (firstChar === '.')
				{
					if (supports)
					{
						if (elm.classList.contains(selector.substr(1)))
						{
							return elm;
						}
					}
					else
					{
						if (new RegExp('(^|\\s)' + selector.substr(1) + '(\\s|$)').test(elm.className))
						{
							return elm;
						}
					}
				}

				// If selector is an ID
				if (firstChar === '#')
				{
					if (elm.id === selector.substr(1))
					{
						return elm;
					}
				}

				// If selector is a data attribute
				if (firstChar === '[')
				{
					if (elm.hasAttribute(attribute[0]))
					{
						if (value)
						{
							if (elm.getAttribute(attribute[0]) === attribute[1])
							{
								return elm;
							}
						}
						else
						{
							return elm;
						}
					}
				}

				// If selector is a tag
				if (elm.tagName.toLowerCase() === selector)
				{
					return elm;
				}
			}
		}
		catch(e) {}

		return null;
	},

	closest : closest
};

dom.hasClass					= dom.containsClass;

export {dom};
